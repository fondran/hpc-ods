#!/usr/bin/bash

RUNTS=$(date "+%Y-%m-%d-%H:%M:%S")
TMPDIR="./tmp"

function log()
{
  TS=$(date "+%Y-%m-%d %H:%M:%S")
  printf "%s\t%s\n" "$TS" "$1"
}


# $1 = A .sif file
# $2 = A directory to create the sandbox (i.e. extract the sif)
function sing_extract()
{
  SIF_FILE=$1
  SB_DIR=$2

  log "sing_extract('$SIF_FILE', '$SB_DIR')"

  if [[ ! -f "$SIF_FILE" ]]
  then
    log "ERROR! SIF file not found : '${SIF_FILE}'\n"
    exit 255
  elif [[ $(basename "$SIF_FILE" ".sif") == "$SIF_FILE" ]]
  then
    log "ERROR! This might not be a sif file: '$SIF_FILE'\n"
    exit 254
  elif [[ -f "$SB_DIR" || -d "$SB_DIR" ]]
  then
    log "ERROR! Directory already exists, or is a regular file : '$SB_DIR'\n"
    exit 253
  fi
  
  LOGFILE="${TMPDIR}/log-extract-${RUNTS}-${PREFIX}.txt"

  if [[ ! -d "$TMPDIR" ]]
  then
    mkdir --verbose --parent "$TMPDIR"
  fi

  log "  SIF File = $SIF_FILE"
  log ""
  log "  Sandbox/ = $SB_DIR"

  if [[ ! -d $(dirname "$SB_DIR") ]]
  then
    mkdir --parent $(dirname "$SB_DIR")
  fi

  ERR=$?
  if [[ $ERR -ne 0 ]]
  then
    printf "ERROR! mkdir returned exit status $ERR\n\n"
  fi

  time \
  singularity build \
    --sandbox \
    --tmpdir="$TMPDIR" \
    --disable-cache \
    "$SB_DIR" \
    "$SIF_FILE" \
    2>&1 | tee --append $LOGFILE
  

  ERR=$?
  printf "\n\n"
  log "Build command returned exit status $ERR\n\n"

  if [[ -d "$SB_DIR" ]]
  then
    chown jrf:jrf "$SB_DIR"
  else
    log "ERROR!  Could not find output sandbox directory : '${SB_DIR}'"
  fi
}

#sing_extract "sif/build-11-update-all.sif" "sandbox/sandbox-11-update-all"

printf "Enter SIF file to extract : "
read SIF

printf "Enter output sandbox directory name : "
read SAND

sing_extract "$SIF" "$SAND"







