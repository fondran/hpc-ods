#!/usr/bin/bash

singularity shell \
  --cleanenv \
  --no-home \
  --writable \
  $@
