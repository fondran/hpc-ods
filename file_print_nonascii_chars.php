#!/usr/bin/php
<?php

if(empty($argv[1]))
{
  die("ERROR: Enter a filename\n");
}
elseif(!file_exists($argv[1]))
{
  die("ERROR: File not found\n");
}

$file = $argv[1];

$fp = fopen($file, 'r');

$i=0;

$format = "%s\t%s\t%s\n";

$counts = array();

while(!feof($fp))
{
  $b = fread($fp, 1);
  
  if($b == "\n")
    $b = "\\n";
  elseif($b == "\t")
    $b = "\\t";
  
//  printf($format, ++$i, $b, ord($b));


  if(array_key_exists($b, $counts))
    $counts[$b]++;
  else
    $counts[$b] = 1;
}

printf(str_repeat("*", 100) . "\n");
$format = "%s\t%s\t%s\t%s\n";

foreach($counts as $b=>$ct)
{
  printf($format, $b, ord($b), $ct, ord($b) > 128 ? "!!!!!" : "");
}

