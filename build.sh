#!/usr/bin/bash
#######################################################################################################################
TMPDIR="/tmp/"
LOGDIR="./log"
SIFDIR="./sif/"

# If 1, then the initial image is created as a sandbox and subsequent .def files build with --update
# Otherwise, build images as defined in the .def file's "From:"
BUILD_WITH_UPDATED_SANDBOX=0

#######################################################################################################################

RUNTS=$(date "+%Y-%m-%d-%H:%M:%S")

function log()
{
  TS=$(date "+%Y-%m-%d %H:%M:%S")
  printf "%s\t%s\n" "$TS" "$*"
}

#######################################################################################################################

SANDBOX_DIR=""
# $1 = .def file
function sing_build()
{
  DEFILE=$1

  log "Starting sing_build('$FILE')"


  if [[ ! -f "$DEFILE" ]]
  then
    log "ERROR! Build file not found : '${DEFILE}'\n"
    exit 255
  fi

  BOOTSTRAP=$(cat "$DEFILE"  | grep "^Bootstrap:" | tr $':' $'\n' | tail -n 1 | sed 's/ //g')
  FROM=$(cat "$DEFILE"  | grep "^From:" | tr $':' $'\n' | tail -n 1 | sed 's/ //g')

  PREFIX=$(basename "$DEFILE" ".def")
  TARGETSIF="${SIFDIR}/${PREFIX}.sif"

  LOGFILE="${LOGDIR}/log-${RUNTS}-${PREFIX}.txt"

  if [[ ! -d "$TMPDIR" ]]
  then
    mkdir --verbose --parent "$TMPDIR"
  fi

  if [[ ! -d "$LOGDIR" ]]
  then
    mkdir --verbose --parent "$LOGDIR"
  fi

  if [[ ! -d "$SIFDIR" ]]
  then
    mkdir --verbose --parent "$SIFDIR"
  fi

  log "  Bootstrap = $BOOTSTRAP"
  log "     Source = $FROM"
  log ""
  log "  .def file = $DEFILE"
  log "     Target = $TARGETSIF"
  log ""
  log "    tmp dir = '$TMPDIR'"
  log "   log file = '$LOGFILE'"

  if [[ "$BOOTSTRAP" == "localimage" && ! -e "$FROM" ]] ##SIF file or sandbox directory
  then
    log "ERROR!  Local image file not found!\n"
    exit 155
  fi

  TMPDIR=$(readlink -f "$TMPDIR")

  if [[ "$BUILD_WITH_UPDATED_SANDBOX" == "1" ]]
  then
    if [[ ${FILE:0:8} == "build-00" ]]
    then
      time singularity build --sandbox \
        --tmpdir="$TMPDIR" \
        "$TARGETSIF" \
        "$DEFILE" \
        2>&1 | tee --append "$LOGFILE"

      SANDBOX_DIR="$TARGETSIF"
    else
      time singularity build --sandbox \
        --update \
        --tmpdir="$TMPDIR"  \
        "$SANDBOX_DIR" \
        "$DEFILE" \
        2>&1 | tee --append "$LOGFILE"
    fi
  else
    time \
    singularity build \
      --tmpdir="$TMPDIR" \
      "$TARGETSIF" \
      "$DEFILE" \
      2>&1 | tee --append "$LOGFILE"
  fi

  ERR=$?

  if [[ $ERR -ne 0 ]]
  then
    log "ERROR! - singularity command returned status $ERR"
    log ""
    exit $ERR
  else
    log "singularity finished with status $ERR"
    log ""
  fi

  log "Finished sing_build('$FILE')"
  log
  log
}

#######################################################################################################################

FILES="
build-00-base-ubuntu.def
build-01-install-apt.def
build-02-install-rstudio.def
build-03-install-r-packages.def
build-04-install-python-modules.def
build-05-install-sdletools.def
build-90-update-all.def
build-91-fixes.def
build-92-more-fixes.def
build-93-yet-more-fixes.def
update-2020-03-26.def
update-2020-03-31.def
update-2020-04-10.def
"

#FILES="update-03-sdletools.def"

for FILE in $FILES
do
  if [[ ${FILE:0:1} == "#" || "$FILE" == "" ]]
  then
    log $(printf "Skipping commented file : %s\n" $FILE)
    continue
  else
    sing_build $FILE
  fi
done






