#!/usr/bin/bash

GROUP=$(groups | tr ' ' $'\n' | head -n 1)

CMD="chown -R jrf16:$GROUP ."

printf "Command : $CMD\n\n"
printf "Press ENTER to continue..."
read X
if [[ $X == "" ]]
then
  printf "Running..."
  sudo $CMD
  printf "done!\n\n"
else
  printf "Doing nothing and exiting\n\n"
  exit 1
fi


