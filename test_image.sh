#!/usr/bin/bash

# Request a non-K40 GPU node, 2G of memory required for tf2_test.py 
srun --x11 --pty --partition=gpu --gres=gpu:1  --exclude=gput025,gput026,gput027,gput028,gput029,gput030 /bin/bash

# Run the container:
module load cuda/10.0
module load singularity
singularity shell --nv build-04-install-python-modules.sif

# In the container:
python3 tf2_test.py



# not anymore
# singularity shell --no-home --cleanenv --bind=./home/user:/home/user:rw build-09-install-python-modules/
# singularity shell --no-home build-09-install-python-modules/




#Cuda
printf "CUDA_VERSION  = ${CUDA_VERSION}\n"
printf "CUDNN_VERSION = ${CUDNN_VERSION}\n" 



#Python
python3 --version

echo "
import tensorflow as tf
print('Tensorflow Version = ' + tf.version.VERSION + '\n')

" | python3


#import pycradletools

#R
R --version | head

R --vanilla <<EOS

library('keras')
library('tensorflow')
tf_version()

library('crack')
library('cradlesgis')
#library('rcradletools') #not expected to work with python3
library('custpairs')
library('matscidat')
library('netSEM')
library('pvcell')
library('rainflow')
library('repomatic')
library('sdletools')
library('solargis')
library('startrpack')
library('uncpow')

q()

EOS





 
