Bootstrap: localimage
From:      sif/build-90-update-all.sif

%files
  package-lists/r-packages-update.R    /scratch/
  package-lists/r-packages-update.txt  /scratch/

%post
  #############################################################################  
  apt-get update
  apt-get --yes install python3-venv

  echo "library(keras); install_keras(); q()" | R --vanilla
  
  # Fixes Qt- & GLX-related errors in RStudio & Spyder
  echo 'export QMLSCENE_DEVICE="softwarecontext rstudio"' >>$SINGULARITY_ENVIRONMENT
  
  ####
  
  ###############################################################################
  # Alternate list of R packages (from SDLE google site)
  Rscript /scratch/r-packages-update.R


  cd /scratch
  #############################################################################  
  #############################################################################
  ## UPDATE - rcradletools
  # !!! libcurl4-openssl-dev is needed so that 'git pull' will work
  #       otherwise, "fatal: Unable to find remote helper for 'https'"
  #apt-get update
  #apt-get install libcurl4-openssl-dev

  #R CMD REMOVE rcradletools
  #cd rcradletools
  #git pull
  #R CMD INSTALL rcradletools/
  
  #rm -rf rcradletools
  #git clone https://sdle:cwru@bitbucket.org/cwrusdle/cradletools/rcradletools
  #R CMD INSTALL rcradletools/rcradletools/
  #rm -rf rcradletools
    
  #############################################################################  
  #############################################################################
  ## INSTALL - Python packages
  #############################################################################
  ### Lasagne - Installed separately below due to error with pip3 install, due to this error :

  # !!! UnicodeDecodeError: 'ascii' codec can't decode byte 0xc3 in position 243: ordinal not in range(128)
  #  The file "CHANGES.rst" contains invalid ascii characters and breaks things.
  LASAGNE_URL="https://files.pythonhosted.org/packages/98/bf/4b2336e4dbc8c8859c4dd81b1cff18eef2066b4973a1bd2b0ca2e5435f35/Lasagne-0.1.tar.gz"
  LASAGNE_TGZ=$(basename "$LASAGNE_URL")
  LASAGNE_DIR=$(basename "$LASAGNE_URL" ".tar.gz")

  curl --output $(basename "$LASAGNE_URL") "$LASAGNE_URL"

  tar xvzf "$LASAGNE_TGZ"
  cd "$LASAGNE_DIR"
  LC_ALL=C tr -dc '\0-\177' <CHANGES.rst >CHANGES.rst.new && mv CHANGES.rst.new CHANGES.rst
  cd ..
  rm "$LASAGNE_TGZ"
  tar cvzf "$LASAGNE_TGZ" "$LASAGNE_DIR" 

  pip3 install \
    --disable-pip-version-check \
    "$LASAGNE_TGZ"

  
  #############################################################################
  ## pymc

  PYMC_URL="https://files.pythonhosted.org/packages/0c/1c/2eb7016284e04ccf401fdcf11b817b160629cf8cf230240ceb348311ac2e/pymc-2.3.6.tar.gz"
  PYMC_TGZ=$(basename "$PYMC_URL")
  PYMC_DIR=$(basename "$PYMC_URL" ".tar.gz")
  
  curl --output "$PYMC_TGZ" "$PYMC_URL"
  
  tar xvzf "$PYMC_TGZ"
  cd "$PYMC_DIR"
  LC_ALL=C tr -dc '\0-\177' <pymc/flib.f >flib.f.new && mv flib.f.new pymc/flib.f
  cd ..
  rm "$PYMC_TGZ"
  tar cvzf "$PYMC_TGZ" "$PYMC_DIR"
  pip3 install \
    --disable-pip-version-check \
    "$PYMC_TGZ"
  
  
  #############################################################################
  # Other, easier, packages...

  pip3 install \
    --disable-pip-version-check \
    config \
    lmfit \
    menpo \
    nltk \
    pygame \
    pystan \
    pywget \
    rdtools \
    scikit-neuralnetwork \
    scikit-plot \
    scikit-survival \
    scikit-video \
    simplejson \
    sphinxcontrib-napoleon2 \
    xgboost
  
  pip3 install \
    --disable-pip-version-check \
    cv2-tools \
    queuelib \
    scikit-learn \
    winreg-helpers \
    xmlrpcssl \
    yaml-1.3




  #pylab   - ERROR: Could not find a version that satisfies the requirement pylab (from versions: none)
  #skimage -  *** Please install the `scikit-image` package (instead of `skimage`) ***

  
  # !!! can't find package !!!
  # backportswifi-oddities :  

  # copyreg  (pip): ? pycopy-copyreg (0.0.0)       - Dummy copyreg module for Pycopy
  #                 ? micropython-copyreg (0.0.0)  - Dummy copyreg module for MicroPython

  # cv2      (pip): ? cv2-tools

  # dateutil (pip): python-dateutil (2.8.1)       - Extensions to the standard Python datetime module
  #                   INSTALLED: 2.8.1 (latest)
  #                 py-dateutil (2.2)             - Extensions to the standard Python datetime module

  # http          :  needs 'request', and then errors

  # libfuturize   : ? futurist

  # libpasteurize : ? 

  # PIL           : try installing again

  # pvimage       :   # pylab         : try installing again

  # pywt    (apt) : python3-pywt - Python3 extension implementing of wavelet transformations

  # queue         : ? queuelib

  # reprlib (pip) : pycopy-reprlib (0.0.1)       - Dummy reprlib module for Pycopy
  #                 micropython-reprlib (0.0.1)  - Dummy reprlib module for MicroPython
  #                 cheap_repr (0.4.1)           - Better version of repr/reprlib for short, cheap string representations.

  # reticulate    : ?

  # skimage (pip) : skimage (0.0)               - Dummy package that points to scikit-image

  # sklearn       : try installing 'scikit-learn' instead

  # socketserver  : micropython-socketserver (0.0.1)  - Dummy socketserver module for MicroPython
  #                 pycopy-socketserver (0.0.1)       - Dummy socketserver module for Pycopy

  # winreg  (pip) : ? winreg-helpers (0.1.1)  - Helper functions for reading/writing to the Windows Registry.

  # xmlrpc        : micropython-xmlrpc (0.0.0)             - Dummy xmlrpc module for MicroPython
  #                 pycopy-xmlrpc (0.0.0)                  - Dummy xmlrpc module for Pycopy

  # yaml          : try install 'yaml-1.3' instead

  # zeallot       : ?

  # m3-PIL        : incompatible with python 3



  # !!! ERROR during install !!!
  # functools32    : DEPRECATED - installation halted with message "This backport is for Python 2.7 only."
  # html           : ERROR - ModuleNotFoundError: No module named 'html.parser'; 'html' is not a package
  # FIXED Lasagne  : FIXED - "UnicodeDecodeError: 'ascii' codec can't decode byte 0xc3 in position 243: ordinal not in range(128)"
  # pymc           : FIXED - "UnicodeDecodeError: 'ascii' codec can't decode byte 0xce in position 5125: ordinal not in range(128)"
  

  #############################################################################  
  #############################################################################
  ## INSTALL - R packages
  #############################################################################
  # Alternate list of R packages (from SDLE google site)
  Rscript /scratch/r-packages-update.R

  cd /scratch

  # Dependencies for SDLE packages

  R --vanilla <<EOS

  myinstall <- function(pkgs)
  {
    install.packages(pkgs, repos="https://cran.case.edu", Ncpus=4, ask=FALSE, dependencies=TRUE)
  }
  
  ## cradlegis dependencies  
  myinstall(c('latex2exp', 'cobs'))

  q()

EOS
  
  # R packages from SDLE

  ## PVplr (previously sdleplr)
  git clone https://sdle:cwru@bitbucket.org/cwrusdle/sdleplr/sdleplr@master
  R CMD INSTALL sdleplr\@master/packages/PVplr
  
  ## SunsVoc
  git clone https://sdle:cwru@bitbucket.org/cwrusdle/sunsvoc/packages/SunsVoc@tyler-tpk
  R CMD INSTALL SunsVoc\@tyler-tpk/packages/SunsVoc

  ## ddiv
  git clone https://sdle:cwru@bitbucket.org/cwrusdle/ddiv/packages/ddiv/ddiv@master
  R CMD INSTALL ddiv\@master/packages/ddiv/ddiv

  ## GeoStats
  URL="http://cg.ensmp.fr/rgeos/DOWNLOAD/RGeostats_11.2.12_linux64.tar.gz"
  FILE=$(basename "$URL")
  DIR=$(basename "$URL" ".tar.gz")
  
  curl -o "$FILE" "$URL"
  tar xvzf "$FILE"
  R CMD INSTALL RGeostats
  
  ## R2I
  URL="http://cg.ensmp.fr/rgeos/DOWNLOAD/R2I_5.5.9_linux64.tar.gz"
  FILE=$(basename "$URL")
  DIR=$(basename "$URL" ".tar.gz")
  
  curl -o "$FILE" "$URL"
  tar xvzf "$FILE"
  R CMD INSTALL R2I
  
  rm -rf /scratch/*
  
# Other fixes:

## pycradletools
  git clone http://sdle:cwru@bitbucket.org/cwrusdle/cradletools/pycradletools@master
  
  cd pycradletools\@master/
  git merge origin/rcradletools_update  
  cd ..

  pip3 install --disable-pip-version-check pycradletools\@master/pycradletools/pycradletools/




##############################
# Python
##############################
%apprun python
  exec python3

##############################
# R
##############################
%apprun R
  exec R

%apprun r
  exec R


##############################
# Spyder
##############################
%apprun spyder
  exec spyder3

%apprun spyder3
  exec spyder3

##############################
# Rstudio
##############################
%apprun rstudio
  exec rstudio



##############################
# foo
##############################
#%apprun foo
#    exec echo "RUNNING FOO"
#%applabels foo
#   BESTAPP FOO
#%appinstall foo
#   touch foo.exec
#%appenv foo
#    SOFTWARE=foo
#    export SOFTWARE
#%apphelp foo
#    This is the help for foo.
#%appfiles foo
#   foo.txt

