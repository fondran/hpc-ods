#!/usr/bin/bash

source ~/.bashrc
source ~/.bash_profile

TMP_DIR="/tmp"
SIF_DIR="./sif"

LATEST_DEF=$(ls -1 update-*.def | tail -n 1)

NEW_SIF=$(basename "$LATEST_DEF" ".def")".sif"

printf "$0 ...\n"
printf "  Latest file appears to be: '%s'\n" "$LATEST_DEF"
printf "  New .sif will be named: '%s/%s'\n" "$SIF_DIR" "$NEW_SIF"
printf "\n"

CMD=$(printf 'singularity build --tmpdir=%s %s %s' $(readlink -f "$TMP_DIR") $(readlink -f "$SIF_DIR/$NEW_SIF") $(readlink -f "$LATEST_DEF"))

printf "  Command to run: %s\n\n" "$CMD"

printf "  Press ENTER to build..."

read FOO

$CMD


